# reconversion-web-pro

## About

This is the final project of the web development bootcamp The Hacking Project. \
It is a team project that has been realized in 2 weeks with Ruby on Rails. \
It is meant to index IT trainings and allow users to find the one he's looking for, depending on its interests, time, location, budget... \

## How to run it

Dependencies you'll probably need to install with your favorite package manager :
* ruby-dev (ruby-devel on Red Hat systems)
* rubygem-bundler
* rubygem-rake
* libpq-devel (postgreSQL client)

```
git clone https://gitlab.com/Ralphiki/reconversion-web-pro.git
cd reconversion-web-pro
bundle install
```

## Links

original repository : https://github.com/Aspinaut/Projet-Final
